
Table of contents
=================

- [Quick start](#quick-start)

- [Running the tests](#running-the-tests)

- [Versioning](#versioning)

- [License](#license)

Quick start
===========

Install with Git
----------------

- Clone the repo: `git clone git@bitbucket.org:dlrg-dev/alertbox.git`

Install with Composer
---------------------

- install [Composer](https://getcomposer.org) if necessary

- add to the `repositories` section in your `composer.json`: 

    ```
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:dlrg-dev/alertbox.git"
        }
    ]
    ```

- add to the `require` section in your `composer.json`:

    ```
    "require": [
        "dlrg/user-interface/alert-box": "~1.1@dev"
    ]
    ```

- run `composer install` or `composer update`

Running the tests
=================

PHPUnit
-------

- run `vendor/bin/phpunit --tests/unit/configuration phpunit.xml` in the project root directory

Versioning
==========

We use [SemVer](http://semver.org/) for versioning.

Licence
=======

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details