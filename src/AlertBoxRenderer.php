<?php

namespace DLRG\UserInterface\Bootstrap\AlertBox;

use InvalidArgumentException;
use RuntimeException;
use DLRG\UserInterface\Bootstrap\AlertBox\AlertBox;
use DLRG\UserInterface\Bootstrap\AlertBox\AlertBoxCollection;
use Twig_Loader_Filesystem;
use Twig_Environment;

/**
 * AlertBoxRenderer
 *
 * @author Manuel Dimmler <dimmler@dlrg.de>
 */
class AlertBoxRenderer
{

    /**
     * the directory for the cached template files
     * @var string
     */
    protected $cacheDirectory;

    /**
     * the AlertBoxCollection to render
     * @var AlertBoxCollection 
     */
    protected $alertBoxCollection;

    /**
     * class constructor
     */
    public function __construct()
    {
        $this->cacheDirectory = ini_get('upload_tmp_dir') | sys_get_temp_dir();
        $this->alertBoxCollection = new AlertBoxCollection();
    }

    /**
     * set a directory for the cached template files
     * @param string $directory
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function setCacheDirectory($directory)
    {
        if (file_exists($directory) === false) {
            throw new InvalidArgumentException('invalide directory');
        }

        if (!is_writable($directory)) {
            throw new RuntimeException('directory isn\'t writeable');
        }

        $this->cacheDirectory = $directory;
    }

    /**
     * add an alert box
     * @param AlertBox $alertBox
     */
    public function addAlertBox(AlertBox $alertBox)
    {
        $this->alertBoxCollection->append($alertBox);
    }
    
    /**
     * add a collection of alert boxes
     * @param AlertBoxCollection $alertBoxCollection
     */
    public function addAlertBoxCollection(AlertBoxCollection $alertBoxCollection)
    {
        foreach($alertBoxCollection as $alertBox) {
            $this->addAlertBox($alertBox);
        }
    }

    /**
     * render the alert boxes and return the rendered html code
     * @return string
     */
    public function render()
    {
        if(count($this->alertBoxCollection) === 0) {
            return '';
        }
        
        $loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/template');
        $templateEngine = new Twig_Environment($loader, array(
            'cache' => $this->cacheDirectory,
            'auto_reload' => true
        ));

        $template = $templateEngine->load('alert-box.html.twig');
        return $template->render(array('alertBoxCollection' => $this->alertBoxCollection));
    }

}
