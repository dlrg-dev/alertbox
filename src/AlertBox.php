<?php

namespace DLRG\UserInterface\Bootstrap\AlertBox;

use InvalidArgumentException;

/**
 * AlertBox
 *
 * @author Manuel Dimmler <dimmler@dlrg.de>
 */
class AlertBox
{

    /**
     * @var string success type
     */
    const TYPE_SUCCESS = 'success';

    /**
     * @var string info type
     */
    const TYPE_INFO = 'info';

    /**
     * @var string warning type
     */
    const TYPE_WARNING = 'warning';

    /**
     * @var string danger type
     */
    const TYPE_DANGER = 'danger';
    
    /**
     * @var string message without formatting
     */
    const MESSAGE_FORMAT_BLANK = 'blank';
    
    /**
     * @var string bullets before each message
     */
    const MESSAGE_FORMAT_LIST_BULLETED = 'list_bulleted';
    
    /**
     * @var string echa message enclosed by a paragraph
     */
    const MESSAGE_FORMAT_PARAGRAPH = 'paragraph';

    /**
     * wheter the alert box can be closed by the user
     * @var boolean 
     */
    protected $closeable = false;

    /**
     * the title of the alert box
     * @var string 
     */
    protected $title = '';

    /**
     * the type / layout of the alert box
     * @var string 
     */
    protected $type = self::TYPE_INFO;

    /**
     * the messages to show in the alert box
     * @var string 
     */
    protected $messageList = [];

    /**
     * the format of the messages in the alert box
     * @var string 
     */
    protected $messageFormat = self::MESSAGE_FORMAT_BLANK;
    
    /**
     * get wether the alert box is closeable
     * @return boolean
     */
    public function isCloseable()
    {
        return $this->closeable;
    }

    /**
     * set wether the alert box is closeable
     * @param boolean $closeable
     * @throws InvalidArgumentException
     */
    public function setCloseable($closeable)
    {
        if (filter_var($closeable, FILTER_VALIDATE_BOOLEAN, array('flags' => FILTER_NULL_ON_FAILURE)) === null) {
            throw new InvalidArgumentException('invalid boolean value');
        }
        $this->closeable = (boolean) $closeable;
    }

    /**
     * get the title of the alert box
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * set the title of the alert box
     * @param string $title
     * @throws InvalidArgumentException
     */
    public function setTitle($title)
    {
        $this->title = (string) $title;
    }

    /**
     * get the type / layout of the alert box
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * set the type / layout of the alert box
     * @param string $type
     * @throws InvalidArgumentException
     */
    public function setType($type)
    {
        switch ($type) {
            case self::TYPE_DANGER:
            case self::TYPE_INFO:
            case self::TYPE_SUCCESS:
            case self::TYPE_WARNING:
                $this->type = $type;
                break;
            default:
                throw new InvalidArgumentException('invalide data type; allowed types are: \'AlertBox::TYPE_DANGER\', \'AlertBox::TYPE_INFO\', \'AlertBox::TYPE_SUCCESS\', \'AlertBox::TYPE_WARNING\'');
        }
    }

    /**
     * get the message of the alert box
     * @return array
     */
    public function getMessageList()
    {
        return $this->messageList;
    }

    /**
     * add a message to the alert box
     * @param string $message
     * @param boolean $allowHtmlCode
     * @throws InvalidArgumentException
     */
    public function addMessage($message, $allowHtmlCode = false)
    {
        if (filter_var($allowHtmlCode, FILTER_VALIDATE_BOOLEAN, array('flags' => FILTER_NULL_ON_FAILURE)) === null) {
            throw new InvalidArgumentException('invalid boolean value for \'$allowHtmlCode\'');
        }
        
        if(empty($message)) {
            throw new InvalidArgumentException('the message cannot be empty');
        }

        if ($allowHtmlCode) {
            $this->messageList[] = $message;
            return;
        }

        $this->messageList[] = filter_var($message, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    }
    
    /**
     * get the format of the messages in the alert box
     * @return string
     */
    public function getMessageFormat()
    {
        return $this->messageFormat;
    }

    /**
     * set the format of the messages in the alert box
     * @param string $format
     * @throws InvalidArgumentException
     */
    public function setMessageFormat($format)
    {
        switch ($format) {
            case self::MESSAGE_FORMAT_BLANK:
            case self::MESSAGE_FORMAT_LIST_BULLETED:
            case self::MESSAGE_FORMAT_PARAGRAPH:
                $this->messageFormat = $format;
                break;
            default:
                throw new InvalidArgumentException('invalide data type; allowed types are: \'AlertBox::MESSAGE_FORMAT_BLANK\', \'AlertBox::MESSAGE_FORMAT_LIST_BULLETED\', \'AlertBox::MESSAGE_FORMAT_PARAGRAPH');
        }
    }
    
    /**
     * validation of the object if all necessary values are given
     * @return boolean
     */
    public function validate() {
        return count($this->messageList) > 0;
    }

}
