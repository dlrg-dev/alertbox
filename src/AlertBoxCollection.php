<?php

namespace DLRG\UserInterface\Bootstrap\AlertBox;

use ArrayIterator;
use DLRG\UserInterface\Bootstrap\AlertBox\AlertBox;
use InvalidArgumentException;
use RuntimeException;

/**
 * AlertBoxCollection
 *
 * @author Manuel Dimmler <dimmler@dlrg.de>
 */
class AlertBoxCollection extends ArrayIterator
{

    /**
     * class constructor
     * @param array $alertBoxList the AlertBox objects to be iterated on
     * @throws InvalidArgumentException
     */
    public function __construct(array $alertBoxList = [])
    {
        foreach ($alertBoxList as $alertBox) {
            if (!($alertBox instanceof AlertBox)) {
                throw new InvalidArgumentException('only instances of \'AlertBox\' are allowed');
            }
            $this->append($alertBox);
        }
    }

    /**
     * append an AlertBox element
     * @param AlertBox $alertBox the AlertBox to append
     * @throws InvalidArgumentException
     */
    public function append($alertBox)
    {
        if (!($alertBox instanceof AlertBox)) {
            throw new InvalidArgumentException('only instances of \'AlertBox\' are allowed');
        }

        if (!$alertBox->validate()) {
            return;
        }
        parent::append($alertBox);
    }

}
