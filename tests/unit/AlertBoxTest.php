<?php

namespace DLRG\UserInterface\AlertBox;

require_once './vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use DLRG\UserInterface\AlertBox\AlertBox;
use InvalidArgumentException;

/**
 * @author Manuel Dimmler <dimmler@dlrg.de>
 * @covers AlertBox
 */
class AlertBoxTest extends TestCase
{

    public function testAlertBoxConstruction()
    {
        $alertBox = new AlertBox();
        $this->assertEquals('DLRG\\UserInterface\\AlertBox\\AlertBox', get_class($alertBox));
    }

    /**
     * @covers AlertBox::setCloseable
     * @covers AlertBox::isCloseable
     */
    public function testSetCloseable()
    {
        $alertBox = new AlertBox();
        $alertBox->setCloseable(true);
        $this->assertEquals(true, $alertBox->isCloseable());
    }

    /**
     * @covers AlertBox::setCloseable
     */
    public function testSetCloseableException()
    {
        $this->expectException(InvalidArgumentException::class);
        $alertBox = new AlertBox();
        $alertBox->setCloseable('invalid type');
    }

    /**
     * @covers AlertBox::setTitle
     * @covers AlertBox::getTitle
     */
    public function testSetTitle()
    {
        $alertBox = new AlertBox();
        $this->assertEquals('', $alertBox->getTitle());
        $alertBox->setTitle('test title');
        $this->assertEquals('test title', $alertBox->getTitle());
        $alertBox->setTitle(0);
        $this->assertEquals('0', $alertBox->getTitle());
        $alertBox->setTitle(true);
        $this->assertEquals('1', $alertBox->getTitle());
        $alertBox->setTitle(null);
        $this->assertEquals('', $alertBox->getTitle());
    }

    /**
     * @covers AlertBox::setType
     * @covers AlertBox::getType
     */
    public function testSetType()
    {
        $alertBox = new AlertBox();
        $alertBox->setType(AlertBox::TYPE_DANGER);
        $this->assertEquals(AlertBox::TYPE_DANGER, $alertBox->getType());
        $alertBox->setType(AlertBox::TYPE_INFO);
        $this->assertEquals(AlertBox::TYPE_INFO, $alertBox->getType());
        $alertBox->setType(AlertBox::TYPE_SUCCESS);
        $this->assertEquals(AlertBox::TYPE_SUCCESS, $alertBox->getType());
        $alertBox->setType(AlertBox::TYPE_WARNING);
        $this->assertEquals(AlertBox::TYPE_WARNING, $alertBox->getType());
    }

    /**
     * @covers AlertBox::setType
     */
    public function testSetTypeException()
    {
        $this->expectException(InvalidArgumentException::class);
        $alertBox = new AlertBox();
        $alertBox->setType('invalid type');
    }

    /**
     * @covers AlertBox::addMessage
     */
    public function testaddMessageExceptionInvalideType()
    {
        $this->expectException(InvalidArgumentException::class);
        $alertBox = new AlertBox();
        $alertBox->addMessage('message', 'invalide type');
    }

    /**
     * @covers AlertBox::addMessage
     */
    public function testaddMessageExceptionEmptyMessage()
    {
        $this->expectException(InvalidArgumentException::class);
        $alertBox = new AlertBox();
        $alertBox->addMessage('');
    }

    /**
     * @covers AlertBox::addMessage
     * @covers AlertBox::getMessageList
     */
    public function testaddMessageWithHtmlDisabled()
    {
        $alertBox = new AlertBox();
        $alertBox->addMessage('<p>first test message</p>');
        $this->assertEquals(1, count($alertBox->getMessageList()));
        $this->assertEquals(htmlspecialchars('<p>first test message</p>'), $alertBox->getMessageList()[0]);
        $alertBox->addMessage('<p>second test message</p>');
        $this->assertEquals(2, count($alertBox->getMessageList()));
    }

    /**
     * @covers AlertBox::addMessage
     * @covers AlertBox::getMessageList
     */
    public function testaddMessageWithHtmlEnabled()
    {
        $alertBox = new AlertBox();
        $alertBox->addMessage('<p>first test message</p>', true);
        $this->assertEquals(1, count($alertBox->getMessageList()));
        $this->assertEquals('<p>first test message</p>', $alertBox->getMessageList()[0]);
        $alertBox->addMessage('<p>second test message</p>', true);
        $this->assertEquals(2, count($alertBox->getMessageList()));
    }

    /**
     * @covers AlertBox::setMessageFormat
     * @covers AlertBox::getMessageFormat
     */
    public function testMessageFormat()
    {
        $alertBox = new AlertBox();
        // default value
        $this->assertEquals(AlertBox::MESSAGE_FORMAT_BLANK, $alertBox->getMessageFormat());

        $alertBox->setMessageFormat(AlertBox::MESSAGE_FORMAT_LIST_BULLETED);
        $this->assertEquals(AlertBox::MESSAGE_FORMAT_LIST_BULLETED, $alertBox->getMessageFormat());

        $alertBox->setMessageFormat(AlertBox::MESSAGE_FORMAT_PARAGRAPH);
        $this->assertEquals(AlertBox::MESSAGE_FORMAT_PARAGRAPH, $alertBox->getMessageFormat());

        $alertBox->setMessageFormat(AlertBox::MESSAGE_FORMAT_BLANK);
        $this->assertEquals(AlertBox::MESSAGE_FORMAT_BLANK, $alertBox->getMessageFormat());
    }

    /**
     * @covers AlertBox::setMessageFormat
     */
    public function testSetMessageFormatException()
    {
        $this->expectException(InvalidArgumentException::class);
        $alertBox = new AlertBox();
        $alertBox->setMessageFormat('invalid type');
    }

    /**
     * @covers AlertBox::setMessageFormat
     */
    public function testValidation()
    {
        $alertBox = new AlertBox();
        $this->assertEquals(false, $alertBox->validate());
        $alertBox->addMessage('test message');
        $this->assertEquals(true, $alertBox->validate());
    }

}
