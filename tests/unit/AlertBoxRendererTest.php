<?php

namespace DLRG\UserInterface\AlertBox;

require_once './vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use DLRG\UserInterface\AlertBox\AlertBox;
use DLRG\UserInterface\AlertBox\AlertBoxCollection;
use InvalidArgumentException;
use RuntimeException;
use org\bovigo\vfs\vfsStream;

/**
 * @author Manuel Dimmler <dimmler@dlrg.de>
 * @covers AlertBoxRenderer
 */
class AlertBoxRendererTest extends TestCase
{

    /**
     * @var  vfsStreamDirectory
     */
    private $rootDirectory;

    public function setUp()
    {
        $this->rootDirectory = vfsStream::setup('tmp');
    }

    /**
     * @covers AlertBoxRenderer::__construct
     */
    public function testAlertBoxRendererConstruction()
    {
        $alertBoxRenderer = new AlertBoxRenderer();
        $this->assertEquals('DLRG\\UserInterface\\AlertBox\\AlertBoxRenderer', get_class($alertBoxRenderer));
    }

    /**
     * @covers AlertBoxRenderer::setCacheDirectory
     */
    public function testSetCacheDirectoryInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);
        $alertBoxRenderer = new AlertBoxRenderer();
        $alertBoxRenderer->setCacheDirectory(vfsStream::url('tmp') . '/invalide');
    }

    /**
     * @covers AlertBoxRenderer::setCacheDirectory
     */
    public function testSetCacheDirectoryRuntimeException()
    {
        $this->expectException(RuntimeException::class);

        $this->rootDirectory->chown(vfsStream::OWNER_ROOT);
        $this->rootDirectory->chmod(0700);

        $alertBoxRenderer = new AlertBoxRenderer();
        $alertBoxRenderer->setCacheDirectory(vfsStream::url('tmp'));
    }

    /**
     * @covers AlertBoxRenderer::setCacheDirectory
     */
    public function testSetCacheDirectory()
    {
        $this->rootDirectory->chown(vfsStream::OWNER_ROOT);
        $this->rootDirectory->chmod(0777);

        $alertBoxRenderer = new AlertBoxRenderer();
        $alertBoxRenderer->setCacheDirectory(vfsStream::url('tmp'));
    }

    /**
     * @covers AlertBoxRenderer::render
     */
    public function testRenderOneAlertBox()
    {

        $alertBoxRenderer = new AlertBoxRenderer();
        $alertBoxRenderer->setCacheDirectory(sys_get_temp_dir());


        $alertBox = new AlertBox();
        $alertBox->addMessage('first test message');

        $alertBoxRenderer->addAlertBox($alertBox);
        $this->assertEquals(1, substr_count($alertBoxRenderer->render(), '<div class="clearfix">'));
        $this->assertEquals(1, substr_count($alertBoxRenderer->render(), 'class="alert-info"'));
    }

    /**
     * @covers AlertBoxRenderer::render
     */
    public function testRenderTwoAlertBoxes()
    {

        $alertBoxRenderer = new AlertBoxRenderer();
        $alertBoxRenderer->setCacheDirectory(sys_get_temp_dir());

        $alertBoxOne = new AlertBox();
        $alertBoxOne->addMessage('first test message');
        $alertBoxRenderer->addAlertBox($alertBoxOne);

        $alertBoxTwo = new AlertBox();
        $alertBoxTwo->addMessage('second test message');
        $alertBoxRenderer->addAlertBox($alertBoxTwo);

        $this->assertEquals(2, substr_count($alertBoxRenderer->render(), '<div class="clearfix">'));
        $this->assertEquals(2, substr_count($alertBoxRenderer->render(), 'role="alert"'));
    }

    /**
     * @covers AlertBoxRenderer::render
     */
    public function testRenderOneAlertBoxTwoMessages()
    {

        $alertBoxRenderer = new AlertBoxRenderer();
        $alertBoxRenderer->setCacheDirectory(sys_get_temp_dir());

        $alertBoxOne = new AlertBox();
        $alertBoxOne->addMessage('first test message');
        $alertBoxOne->addMessage('second test message');
        $alertBoxRenderer->addAlertBox($alertBoxOne);

        $this->assertEquals(2, substr_count($alertBoxRenderer->render(), '<div class="clearfix">'));
        $this->assertEquals(1, substr_count($alertBoxRenderer->render(), 'role="alert"'));
    }

    /**
     * @covers AlertBoxRenderer::render
     */
    public function testRenderTypes()
    {

        $alertBoxRenderer = new AlertBoxRenderer();
        $alertBoxRenderer->setCacheDirectory(sys_get_temp_dir());

        $alertBoxOne = new AlertBox();
        $alertBoxOne->addMessage('test message one');
        $alertBoxOne->setType(AlertBox::TYPE_DANGER);
        $alertBoxRenderer->addAlertBox($alertBoxOne);

        $alertBoxTwo = new AlertBox();
        $alertBoxTwo->addMessage('test message two');
        $alertBoxTwo->setType(AlertBox::TYPE_INFO);
        $alertBoxRenderer->addAlertBox($alertBoxTwo);

        $alertBoxThree = new AlertBox();
        $alertBoxThree->addMessage('test message three');
        $alertBoxThree->setType(AlertBox::TYPE_SUCCESS);
        $alertBoxRenderer->addAlertBox($alertBoxThree);

        $alertBoxFour = new AlertBox();
        $alertBoxFour->addMessage('test message four');
        $alertBoxFour->setType(AlertBox::TYPE_WARNING);
        $alertBoxRenderer->addAlertBox($alertBoxFour);

        $this->assertEquals(1, substr_count($alertBoxRenderer->render(), 'class="alert-danger"'));
        $this->assertEquals(1, substr_count($alertBoxRenderer->render(), 'class="alert-info"'));
        $this->assertEquals(1, substr_count($alertBoxRenderer->render(), 'class="alert-success"'));
        $this->assertEquals(1, substr_count($alertBoxRenderer->render(), 'class="alert-warning"'));
    }

    /**
     * @covers AlertBoxRenderer::render
     */
    public function testRenderIsCloseable()
    {

        $alertBoxRenderer = new AlertBoxRenderer();
        $alertBoxRenderer->setCacheDirectory(sys_get_temp_dir());

        $alertBoxOne = new AlertBox();
        $alertBoxOne->addMessage('test message one');
        $alertBoxOne->setCloseable(true);
        $alertBoxRenderer->addAlertBox($alertBoxOne);

        $alertBoxTwo = new AlertBox();
        $alertBoxTwo->addMessage('test message two');
        $alertBoxTwo->setCloseable(false);
        $alertBoxRenderer->addAlertBox($alertBoxTwo);

        $this->assertEquals(1, substr_count($alertBoxRenderer->render(), '<button'));
    }

    /**
     * @covers AlertBoxRenderer::render
     */
    public function testRenderMessageFormat()
    {

        $alertBoxRenderer = new AlertBoxRenderer();
        $alertBoxRenderer->setCacheDirectory(sys_get_temp_dir());

        $alertBoxOne = new AlertBox();
        $alertBoxOne->addMessage('test message one');
        $alertBoxOne->setMessageFormat(AlertBox::MESSAGE_FORMAT_BLANK);
        $alertBoxRenderer->addAlertBox($alertBoxOne);

        $alertBoxTwo = new AlertBox();
        $alertBoxTwo->addMessage('test message two');
        $alertBoxTwo->setMessageFormat(AlertBox::MESSAGE_FORMAT_LIST_BULLETED);
        $alertBoxRenderer->addAlertBox($alertBoxTwo);

        $alertBoxThree = new AlertBox();
        $alertBoxThree->addMessage('test message three');
        $alertBoxThree->setMessageFormat(AlertBox::MESSAGE_FORMAT_PARAGRAPH);
        $alertBoxRenderer->addAlertBox($alertBoxThree);

        $this->assertEquals(1, substr_count($alertBoxRenderer->render(), '<li>'));
        $this->assertEquals(1, substr_count($alertBoxRenderer->render(), '<p>'));
    }

    /**
     * @covers AlertBoxRenderer::render
     */
    public function testRenderTitle()
    {

        $alertBoxRenderer = new AlertBoxRenderer();
        $alertBoxRenderer->setCacheDirectory(sys_get_temp_dir());

        $alertBoxOne = new AlertBox();
        $alertBoxOne->addMessage('test message one');
        $alertBoxOne->setTitle('test title');
        $alertBoxRenderer->addAlertBox($alertBoxOne);

        $this->assertEquals(1, substr_count($alertBoxRenderer->render(), 'test title'));
    }

    /**
     * @covers AlertBoxRenderer::render
     */
    public function testRenderOneAlertBoxCollection()
    {

        $alertBoxRenderer = new AlertBoxRenderer();
        $alertBoxRenderer->setCacheDirectory(sys_get_temp_dir());

        $alertBoxCollection = new AlertBoxCollection();

        $alertBoxOne = new AlertBox();
        $alertBoxOne->addMessage('test message one');
        $alertBoxOne->setTitle('test title');
        $alertBoxCollection->append($alertBoxOne);

        $alertBoxTwo = new AlertBox();
        $alertBoxTwo->addMessage('test message two');
        $alertBoxTwo->setMessageFormat(AlertBox::MESSAGE_FORMAT_PARAGRAPH);
        $alertBoxCollection->append($alertBoxTwo);

        $alertBoxRenderer->addAlertBoxCollection($alertBoxCollection);

        $this->assertEquals(1, substr_count($alertBoxRenderer->render(), 'test title'));
        $this->assertEquals(1, substr_count($alertBoxRenderer->render(), '<p>'));
    }

}
