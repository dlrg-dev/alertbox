<?php

namespace DLRG\UserInterface\AlertBox;

require_once './vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use DLRG\UserInterface\AlertBox\AlertBox;
use DLRG\UserInterface\AlertBox\AlertBoxCollection;
use InvalidArgumentException;
use RuntimeException;
use stdClass;

/**
 * @author Manuel Dimmler <dimmler@dlrg.de>
 * @covers AlertBoxCollection
 */
class AlertBoxCollectionTest extends TestCase
{

    /**
     * @covers AlertBoxCollection::__construct
     */
    public function testAlertBoxCollectionConstruction()
    {
        $alertBoxCollection = new AlertBoxCollection();
        $this->assertEquals('DLRG\\UserInterface\\AlertBox\\AlertBoxCollection', get_class($alertBoxCollection));

        $this->assertEquals(false, $alertBoxCollection->valid());
    }

    /**
     * @covers AlertBoxCollection::__construct
     */
    public function testAlertBoxCollectionConstructionException()
    {
        $this->expectException(InvalidArgumentException::class);
        $alertBoxCollection = new AlertBoxCollection([new stdClass()]);
    }

    /**
     * @covers AlertBoxCollection::append
     */
    public function testAppendInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);
        $alertBoxCollection = new AlertBoxCollection();
        $alertBoxCollection->append(new stdClass());
    }

    /**
     * @covers AlertBoxCollection::append
     */
    public function testAppendEmptyAlertBox()
    {
        $alertBoxCollection = new AlertBoxCollection();
        $alertBoxCollection->append(new AlertBox());
        $this->assertEquals(0, $alertBoxCollection->count());
    }

    /**
     * @covers AlertBoxCollection::append
     */
    public function testAppend()
    {
        $alertBoxCollection = new AlertBoxCollection();

        $alertBox1 = new AlertBox();
        $alertBox1->addMessage('first test message');
        $alertBoxCollection->append($alertBox1);

        $this->assertEquals(true, $alertBoxCollection->valid());
        $this->assertEquals($alertBox1, $alertBoxCollection->current());

        $alertBox2 = new AlertBox();
        $alertBox2->addMessage('second test message');
        $alertBoxCollection->append($alertBox2);

        $alertBoxCollection->next();
        $this->assertEquals($alertBox2, $alertBoxCollection->current());
    }

}
